from aiogram import Bot, Dispatcher, F
from aiogram.filters import Command
from aiogram.types import Message, ContentType
from config import BOT_TOKEN

bot = Bot(token=BOT_TOKEN)
dp = Dispatcher()


# хендлер для обработки команду /start
@dp.message(Command(commands=['start']))
async def process_start_command(message: Message):
    # посмотреть на пришедший апдейт
    print(message.model_dump_json(indent=4, exclude_none=True))
    await message.answer(
        text="""
        Напиши мне что-нибудь
        """
    )


# Этот хендлер будет обрабатывать команду /help
@dp.message(Command(commands=['help']))
async def process_help_command(message: Message):
    await message.answer(
        text='Напиши че нить'
    )


# Этот хендлер будет обрабатывать апдейты с фотками
@dp.message(F.content_type == ContentType.PHOTO)
async def process_photo_command(message: Message):
    await message.reply_photo(
        photo=message.photo[0].file_id
    )


# Этот хендлер будет срабатывать на любые сообщения
# кроме /start и /help
@dp.message()
async def send_echo(message: Message):
    await message.reply(text=message.text)


if __name__ == "__main__":
    dp.run_polling(bot)