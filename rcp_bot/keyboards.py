from aiogram.types import ReplyKeyboardMarkup, ReplyKeyboardRemove, KeyboardButton

button_rock: list = [KeyboardButton(text='Камень')]
button_scissors: list = [KeyboardButton(text='Ножницы')]
button_paper: list = [KeyboardButton(text='Бумага')]

keyboard_to_play: ReplyKeyboardMarkup = ReplyKeyboardMarkup(
    keyboard=
    [
        button_rock,
        button_scissors,
        button_paper
    ],
    resize_keyboard=True,
    one_time_keyboard=True
)

button_yes = [KeyboardButton(text='Давай!')]
button_no = [KeyboardButton(text='Не хочу!')]

keyboard = ReplyKeyboardMarkup(
    keyboard=
    [
        button_yes,
        button_no
    ],
    resize_keyboard=True,
    one_time_keyboard=True
)
