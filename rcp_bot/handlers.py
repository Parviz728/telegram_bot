from random import choice
from aiogram import F, Router
from aiogram.filters import Command
from aiogram.types import Message
from lexicon import COMMANDS
from keyboards import keyboard, keyboard_to_play

router = Router()


def check_who_wins(user_text: str, bot_text: str) -> str:
    if user_text == 'Камень':
        if bot_text == 'Камень':
            return 'Играем дальше!\nЖми на кнопку!'
        elif bot_text == 'Ножницы':
            return 'Ты победил!'
        elif bot_text == 'Бумага':
            return 'Ты проиграл!'
    elif user_text == 'Ножницы':
        if bot_text == 'Камень':
            return 'Ты проиграл!'
        elif bot_text == 'Ножницы':
            return 'Играем дальше!\nЖми на кнопку!'
        elif bot_text == 'Бумага':
            return 'Ты победил!'
    elif user_text == 'Бумага':
        if bot_text == 'Камень':
            return 'Ты победил!'
        elif bot_text == 'Ножницы':
            return 'Ты проиграл!'
        elif bot_text == 'Бумага':
            return 'Играем дальше!\nЖми на кнопку!'


@router.message(Command(commands=['start']))
async def process_start_command(message: Message):
    await message.answer(
        text=COMMANDS['/start'],
        reply_markup=keyboard
    )


@router.message(Command(commands=['help']))
async def process_start_command(message: Message):
    await message.answer(
        text=COMMANDS['/help']
    )


@router.message(F.text == 'Давай!')
async def process_yes_command(message: Message):
    await message.answer(
        text=COMMANDS['Давай!'],
        reply_markup=keyboard_to_play
    )


@router.message(F.text.in_(['Камень', 'Ножницы', 'Бумага']))
async def process_rock_command(message: Message):
    bot_ans = choice(['Камень', 'Ножницы', 'Бумага'])
    user_text = message.text
    res = check_who_wins(user_text=user_text, bot_text=bot_ans)
    answer = f"{bot_ans}\n{res}\nХочешь сыграть ещё?"
    ans_keyboard = keyboard
    if res == 'Играем дальше!\nЖми на кнопку!':
        answer = f"{bot_ans}\n{res}"
        ans_keyboard = keyboard_to_play
    await message.answer(
        text=answer,
        reply_markup=ans_keyboard
    )


@router.message(F.text == 'Не хочу!')
async def process_no_command(message: Message):
    await message.answer(
        text=COMMANDS['Не хочу!']
    )


@router.message()
async def process_start_command(message: Message):
    await message.answer(
        text='Извините\nЯ умею только играть в камень, ножницы, бумага'
    )