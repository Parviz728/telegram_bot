import asyncio
from random import randint
from aiogram import Bot, Dispatcher, F
from config import BOT_TOKEN
from aiogram.types import Message, KeyboardButton, ReplyKeyboardMarkup
from aiogram.filters import Command
from menu import set_menu
import logging

logger = logging.getLogger(__name__)

bot = Bot(token=BOT_TOKEN)
dp = Dispatcher()
ATTEMPTS = 5
users = {}
# Создаем объекты кнопок
button_1 = KeyboardButton(text='Да')
button_2 = KeyboardButton(text='Нет')

# Создаем объект клавиатуры, добавляя в него кнопки
keyboard_markup = ReplyKeyboardMarkup(keyboard=[[button_1, button_2]], resize_keyboard=True)
secret_number = 0


@dp.message(Command(commands=['start']))
async def process_start_command(message: Message):
    await message.answer(
        text=
        f'Привет {message.from_user.first_name}!\n'
        f'Давай сыграем в игру "Угадай число"\n'
        f'Чтобы посмотреть правила игры введи /rules'
    )
    user_id = message.from_user.id
    if user_id not in users:
        users[user_id] = {
            'in_game': None,
            'secret_number': 0,
            'attempts': ATTEMPTS,
            'total_games': 0,
            'wins': 0
        }


@dp.message(Command(commands=['rules']))
async def process_rules_command(message: Message):
    await message.answer(
        text=
        f'Я загадываю целое число от 1 до 100\n'
        f'Твоя задача его угадать\n'
        f'Пиши в этот чат числа, а я скажу мое число больше или меньше\n'
        f'У тебя будет 5 попыток. Играем?',
        reply_markup=keyboard_markup
    )


@dp.message(F.text == 'Да')
async def process_yes_command(message: Message):
    global secret_number
    secret_number = randint(1, 100)
    user_id = message.from_user.id
    users[user_id]['secret_number'] = secret_number
    users[user_id]['in_game'] = True
    users[user_id]['total_games'] += 1
    await message.answer(
        text="Отлично!\nЯ загадал число, у тебя 5 попыток его угадать!"
    )


@dp.message(F.text == 'Нет')
async def process_yes_command(message: Message):
    await message.answer(
        text="Чмо"
    )


# Этот хэндлер будет срабатывать на отправку пользователем чисел от 1 до 100
@dp.message(lambda x: x.text and x.text.isdigit() and 1 <= int(x.text) <= 100)
async def process_number_command(message: Message):
    user_id = message.from_user.id
    guess = int(message.text)
    if users[user_id]['attempts'] > 1:
        if guess < secret_number:
            await message.answer(
                text="Загаданное число больше"
            )
        elif guess > secret_number:
            await message.answer(
                text="Загаданное число меньше"
            )
        else:
            await message.answer(
                text="Поздравляю!\nТы выиграл!\nХочешь ещё?",
                reply_markup=keyboard_markup
            )
            users[user_id]['in_game'] = False
            users[user_id]['wins'] += 1
        users[user_id]['attempts'] -= 1
    else:
        if guess == secret_number:
            await message.answer(
                text="Поздравляю!\nТы выиграл!\nХочешь ещё?",
                reply_markup=keyboard_markup
            )
            users[user_id]['in_game'] = False
            users[user_id]['wins'] += 1
            users[user_id]['in_game'] = False
        else:
            await message.answer(
                text=f"Увы, мимо!\nЭто была твоя последняя попытка\n"
                     f"Правильный ответ был {secret_number}\n"
                     f"Хочешь отыграться?",
                reply_markup=keyboard_markup
            )
            users[user_id]['in_game'] = False


@dp.message(Command(commands=['stat']))
async def process_stat_command(message: Message):
    user_id = message.from_user.id
    if users[user_id]['total_games'] != 0:
        res = (users[user_id]['wins'] / users[user_id]['total_games']) * 100
        await message.answer(
            text=f"У тебя {users[user_id]['total_games']} игр и {users[user_id]['wins']} побед\nТвоя статистика побед: {res}%"
        )
    else:
        await message.answer(
            text=f'Чушпан!\nТы еще ни разу не играл! Какую статистику ты хочешь увидеть??'
        )


@dp.message()
async def process_other_answers(message: Message):
    if users[message.from_user.id]['in_game']:
        await message.answer(
            'Мы же сейчас играем.\n'
            'Присылай числа от 1 до 100'
        )
    else:
        await message.answer(
            'Я довольно ограниченный бот, давай\n'
            'просто сыграем в игру?'
        )


async def main():
    # Конфигурируем логирование
    logging.basicConfig(
        level=logging.INFO,
        format='%(filename)s:%(lineno)d #%(levelname)-8s '
               '[%(asctime)s] - %(name)s - %(message)s')

    # Выводим в консоль информацию о начале запуска бота
    logger.info('Starting bot')

    await set_menu(bot)

    # Пропускаем накопившиеся апдейты и запускаем polling
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)

asyncio.run(main())