import random
import numpy as np
import cv2
from aiogram import Bot, Dispatcher, F
from aiogram.filters import Command
from aiogram.types import Message
from config import BOT_TOKEN
import argparse
from io import BytesIO

# подключаем парсер аргументов командной строки
parser = argparse.ArgumentParser()
# добавляем аргумент для работы с изображениями
parser.add_argument('--image')
# сохраняем аргумент в отдельную переменную
args = parser.parse_args()
# прописываем цвет по умолчанию
color = (0, 255, 0)

bot = Bot(token=BOT_TOKEN)
dp = Dispatcher()
user = {}


# def get_random_number() -> int:
#     return random.randint(1, 100)


def highlightFace(net, frame, conf_threshold=0.7):
    # делаем копию текущего кадра
    frameOpencvDnn = frame.copy()
    # высота и ширина кадра
    frameHeight = frameOpencvDnn.shape[0]
    frameWidth = frameOpencvDnn.shape[1]
    # преобразуем картинку в двоичный пиксельный объект
    blob = cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (300, 300), [104, 117, 123], True, False)
    # устанавливаем этот объект как входной параметр для нейросети
    net.setInput(blob)
    # выполняем прямой проход для распознавания лиц
    detections = net.forward()
    # переменная для рамок вокруг лица
    faceBoxes = []
    # перебираем все блоки после распознавания
    for i in range(detections.shape[2]):
        # получаем результат вычислений для очередного элемента
        confidence = detections[0, 0, i, 2]
        # если результат превышает порог срабатывания — это лицо
        if confidence > conf_threshold:
            # формируем координаты рамки
            x1 = int(detections[0, 0, i, 3] * frameWidth)
            y1 = int(detections[0, 0, i, 4] * frameHeight)
            x2 = int(detections[0, 0, i, 5] * frameWidth)
            y2 = int(detections[0, 0, i, 6] * frameHeight)
            # добавляем их в общую переменную
            faceBoxes.append([x1, y1, x2, y2])
            # рисуем рамку на кадре
            cv2.rectangle(frameOpencvDnn, (x1, y1), (x2, y2), color, int(round(frameHeight / 150)), 8)
    # возвращаем кадр с рамками
    return frameOpencvDnn, faceBoxes


# загружаем веса для распознавания лиц
faceProto = "opencv_face_detector.pbtxt"
# и конфигурацию самой нейросети — слои и связи нейронов
faceModel = "opencv_face_detector_uint8.pb"
# точно так же загружаем модели для определения пола и возраста
genderProto = "gender_deploy.prototxt"
genderModel = "gender_net.caffemodel"
ageProto = "age_deploy.prototxt"
ageModel = "age_net.caffemodel"

# настраиваем свет
MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
# итоговые результаты работы нейросетей для пола и возраста
genderList = ['Парень', 'Девушка']
ageList = \
    [
        # '0-2', '2-4' '4-6', '6-8',
        # '8-12', '12-15', '15-18',
        '18-22', '22-25'
        # '25-32', '32-38' '38-43', '48-53'
    ]

# запускаем нейросеть по распознаванию лиц
faceNet = cv2.dnn.readNet(faceModel, faceProto)
# и запускаем нейросети по определению пола и возраста
genderNet = cv2.dnn.readNet(genderModel, genderProto)
ageNet = cv2.dnn.readNet(ageModel, ageProto)


def analyze_image(image):
    frame = cv2.imdecode(np.frombuffer(image, np.uint8), cv2.IMREAD_COLOR)
    resultImg, faceBoxes = highlightFace(faceNet, frame)
    results = []
    for faceBox in faceBoxes:
        face = frame[max(0, faceBox[1]):min(faceBox[3], frame.shape[0] - 1),
               max(0, faceBox[0]):min(faceBox[2], frame.shape[1] - 1)]
        blob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), MODEL_MEAN_VALUES, swapRB=False)

        genderNet.setInput(blob)
        genderPreds = genderNet.forward()
        gender = genderList[genderPreds[0].argmax()]

        ageNet.setInput(blob)
        agePreds = ageNet.forward()
        # age = ageList[agePreds[0].argmax()]
        age = random.choice(['18-22', '22-25'])
        results.append((gender, age))
    return results


async def process_start_command(message: Message):
    print(message.model_dump_json(indent=4, exclude_none=True))
    await message.answer(f"""Здравствуйте, {message.chat.first_name}!
Пришлите мне свою фотографию и я попробую угадать ваш возраст""")


k = 0


# Этот хэндлер будет срабатывать на отправку боту фото
async def send_photo_echo(message: Message):
    global k
    k += 1
    photo = message.photo[-1]
    photo_file = await bot.download(photo.file_id, BytesIO())
    photo_bytes = photo_file.read()

    results = analyze_image(photo_bytes)
    if results:
        response = ""
        for gender, age in results:
            if k < 3:
                gender = 'Парень'
            else:
                gender = 'Девушка'
            response += f'Пол: {gender}\nВозраст: {age}\n'
        print(f"Имя: {message.chat.first_name}\n{response}")
        await message.reply(response)
    else:
        await message.reply('Не удалось распознать лицо на фотографии.')
    # await message.reply_photo(message.photo[0].file_id)

dp.message.register(process_start_command, Command(commands=['start']))
dp.message.register(send_photo_echo, F.photo)

if __name__ == "__main__":
    dp.run_polling(bot)